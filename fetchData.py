import pandas as pd
from datetime import timedelta, datetime
import requests
from io import StringIO
import json
import time

timeSteps = 36      # one hour one step

columnDtypes = {
    'campaign_id' : 'uint32',
    'zone_id' : 'uint32',
    'browser_id' : 'uint8',
    'os_id' : 'uint8',
    'device_id' : 'uint8',
    'country_code' : 'object',
    'w_imps' : 'float64',
    'w_clicks' : 'float64',
    'w_income' : 'float64',
    'w_eRPM' : 'float64',
    'w_CTR' : 'float64',
    'w_CPC' : 'float64'
}

userData = {
    "username" : 'fenix@coinis.com',
    "password" : 'ML$[Y/Zwu$>!3U9@'
}

#---------- Acquire dataset ----------#
print('Fetching data...')

startTime = time.time()

r = requests.post("http://api.ocamba.com/v2/ocamba/auth", data = json.dumps(userData), headers = {"Content-Type": "application/json"})
response = json.loads(r.text)
accessToken = response["token"]
authString = 'Bearer ' + accessToken
headers = {
    'Authorization': authString,
    'Content-type': 'application/json'
}

endDate = datetime.now()
startDate = endDate - timedelta(hours = timeSteps)

request = ("http://api.ocamba.com/v1/reports/adex/exchange?dimensions=stat_date,"
                       "campaign_id,zone_id,browser_id,country_code,os_id,device_id"
                       "&measures=impression,total_click,income"
                       "&impression=gt:0"
                       "&stat_date=rl:" + startDate.strftime('%Y-%m-%d %H:00:00') + "," + endDate.strftime('%Y-%m-%d %H:00:00'))

acquiredData = requests.get(request, headers=headers)
print(f'Data acquired in {time.time() - startTime} seconds.')

#%%
s = str(acquiredData.content,'utf-8')
data = StringIO(s)
df = pd.read_csv(data, dtype = columnDtypes, keep_default_na = False, na_values = [''])

outputFilePath = 'datasets/'
df.to_csv(outputFilePath + 'campaigns_all.csv', encoding='utf-8')
