#include <iostream>
#include <fstream>
#include "include/json.hpp"
#include "include/campaigns.h"

using namespace std;
using json = nlohmann::json;
using namespace coinis;

int main () {
    string inputFile = "results\\userCampaigns.json";    
    ifstream inputFileStream(inputFile);
    json jsonUsers = json::parse(inputFileStream);

    // Section which sorts users by means of their campaigns
    std::vector<UserCampaignsMeans> userCampsMeans;
    for (json::iterator it = jsonUsers.begin(); it != jsonUsers.end(); ++it) {
        string userCampaignsString = jsonUsers[it.key()];
        userCampsMeans.push_back(UserCampaignsMeans(it.key(), userCampaignsString, '@', '#'));
    }

    sort(userCampsMeans.begin(), userCampsMeans.end(), [](const UserCampaignsMeans& lhs, const UserCampaignsMeans& rhs) {
        return (lhs.nonZeroMean > rhs.nonZeroMean) || 
               (lhs.nonZeroMean == rhs.nonZeroMean && lhs.meanOfBetas > rhs.meanOfBetas);
    });

    for(int i = 700; i < 900; i++)
        std::cout << userCampsMeans[i].userId << " nonZero: " << 
                     userCampsMeans[i].nonZeroMean << " zero: " << 
                     userCampsMeans[i].zeroMean << endl;
}