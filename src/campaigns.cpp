#include <iostream>
#include <fstream>
#include <vector>
#include <random>
#include <ctime>
#include <ratio>
#include <chrono>
#include "include/csv.h"
#include "include/json.hpp"
#include "include/campaigns.h"

using namespace std;
using namespace std::chrono;
using json = nlohmann::json;

namespace coinis {
    
    /////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////   Members of class CampaignData   /////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////

    int CampaignData::timeStepsMax = 48;

    CampaignData::CampaignData() {}

    CampaignData::CampaignData(int _timeStepsMax) {
        timeSteps = 0;
        impressions.resize(_timeStepsMax);
        income.resize(_timeStepsMax);
    }

    array<double, 3> CampaignData::getParameters() {
        array<double, 3> params = {mu_0, alpha, beta};
        return params;
    }

    void CampaignData::insertData(unsigned long currentImpressions, double currentIncome) {
        try {
            impressions.at(timeSteps) = currentImpressions;
            income.at(timeSteps) = currentIncome;
            timeSteps++;
        }
        catch (const std::out_of_range &oor) {
            cerr << "Out of range error: " << oor.what() << endl;
        }
    }

    string CampaignData::getCampaignDataString() {
        char dataArray[1000] = "[", temp[30];
        for(int i = 0; i < timeSteps; i++){
            sprintf(temp, "%g%s", calculateReward(impressions[i], income[i]), (i < timeSteps - 1) ? ", " : "");
            strcat(dataArray, temp);
        }
        strcat(dataArray, "]");
        return dataArray;
    }

    void CampaignData::calculateCampaignParameters(const ThomsonSamplingParams _tsParams) {
        mu_0 = 0.0;
        for(int i = 0; i < timeSteps; i++)
            mu_0 += calculateReward(impressions[i], income[i]);
        mu_0 /= timeSteps;

        alpha = _tsParams.alphaInit + _tsParams.alphaScale * timeSteps;
        beta = _tsParams.betaInit;
        double sample;
        for(int i = 0; i < timeSteps; i++) {
            sample = calculateReward(impressions[i], income[i]);
            beta += 0.5 * (sample - mu_0) * (sample - mu_0);
        }
    }

    inline double CampaignData::calculateReward(double _impressions, double _income) {
        return _income * 1000.0 / _impressions;
    }

    void CampaignData::printCampaignParameters() {
        cout << "mu_0: " << mu_0 << ", alpha: " << alpha << ", beta: " << beta << endl;
    }

    //////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////   Members of class CampaignUsers   /////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////
    
    CampaignUsers::CampaignUsers() {}

    CampaignUsers::CampaignUsers(string inputFile) {
        fillFromDataset(inputFile);
    }

    unordered_map<string, unordered_map<string, CampaignData>> &CampaignUsers::getMap() {
        return users;
    }

    void CampaignUsers::fillFromDataset(string inputFile) {
        string countryCode, campaignId, zoneId, browserId, osId, deviceId, userId;
        unsigned long impression;
        double income;
        io::CSVReader<8> csvReader(inputFile);
        csvReader.read_header(io::ignore_extra_column, "campaign_id", "zone_id", "browser_id", "country_code", "os_id", "device_id", "impression", "income");

        while(csvReader.read_row(campaignId, zoneId, browserId, countryCode, osId, deviceId, impression, income)) {
            userId = zoneId + "_" + browserId + "_" + countryCode + "_" + osId + "_" + deviceId;
            if(users.count(userId) ==  0)
                addNewUser(userId, campaignId);
            else if(users[userId].count(campaignId) == 0)
                addNewCampaign(userId, campaignId);
            updateCampagnData(userId, campaignId, impression, income);
        }
    } 

    void CampaignUsers::printUsers() {
        for(auto &pair: users)
            cout << pair.first << endl;
    }

    void CampaignUsers::printAllUserCampaigns() {
        for(auto &user: users) {
            cout << user.first << endl;
            printCampaignsOfUser(user.first);
        }
    }

    void CampaignUsers::printCampaignsOfUser(string _userId) {
        for(auto campaign: users[_userId])
            cout << campaign.first << " ";
        cout << endl;
    }

    void CampaignUsers::printCampaignDataOfAllUsers(string fileName) {
        ofstream dataFile;
        dataFile.open("results/" + fileName);
        
        for(auto &user: users) {
            std::cout << user.first << endl;
            for(auto &campaign: users[user.first]) {
                std::string campaignDataString = users[user.first][campaign.first].getCampaignDataString();
                dataFile << "userMap(\'" << campaign.first << "\') = " << campaignDataString << ";" << endl;
            }
        }
        dataFile.close();
    }

    void CampaignUsers::addNewUser(string _userId, string _campaignId) {
        CampaignData newDataSeries(CampaignData::timeStepsMax);
        unordered_map<string, CampaignData> newUserCampaign;
        newUserCampaign.insert({_campaignId, newDataSeries});
        users.insert({_userId, newUserCampaign});
    }

    void CampaignUsers::addNewCampaign(string _userId, string _campaignId) {
        CampaignData newDataSeries(CampaignData::timeStepsMax);
        users[_userId].insert({_campaignId, newDataSeries});
    }

    void CampaignUsers::updateCampagnData(string _userId, string _campaign_id, unsigned long _impressions, double _income) {
        users[_userId][_campaign_id].insertData(_impressions, _income);
    }

    void CampaignUsers::calculateParametersOfAllCampaigns(const ThomsonSamplingParams _tsParams) {
        for(auto &user: users)
            for(auto &campaign: users[user.first])
                users[user.first][campaign.first].calculateCampaignParameters(_tsParams);
    }

    void CampaignUsers::printParametersOfAllCampaigns() {
        for(auto &user: users) {
            cout << user.first << endl;
            for(auto &campaign: users[user.first]) {
                cout << campaign.first << ": ";
                users[user.first][campaign.first].printCampaignParameters();
            }
        }
    }

    void CampaignUsers::exportCampaigns(string outputFileName) {
        // json jsonUsers = createJsonData();
        json jsonUsers = createJsonDataStringified();
        printJsonToFile(jsonUsers, outputFileName);
    }

    json CampaignUsers::createJsonData() {
        // First realization, with complete json structure (each campaign one node)
        json jsonUsers;
        for(auto &user: users) {
            json userCampaigns = json::object();
            for(auto &campaign: users[user.first]) {
                array<double, 3> params = users[user.first][campaign.first].getParameters();
                userCampaigns[campaign.first] = {params[0], params[1], params[2]};
            }
            jsonUsers[user.first] = userCampaigns;
        }
        return jsonUsers;
    }

    json CampaignUsers::createJsonDataStringified() {
        json jsonUsers;
        for(auto &user: users) {
            std::string userCampaignsString;
            for(auto &campaign: users[user.first]) {
                std::string campaignDataString = stringifyCampaignData(campaign.first, users[user.first][campaign.first]);
                userCampaignsString += campaignDataString;
            }
            userCampaignsString.pop_back();
            jsonUsers[user.first] = userCampaignsString;
        }
        return jsonUsers;
    }

    void CampaignUsers::printJsonToFile(json &_jsonUsers, string outputFile) {
        std::ofstream file(outputFile, std::ios::out);
        file << _jsonUsers;
    }

    string CampaignUsers::stringifyCampaignData(string campaignId, CampaignData campaignData) {
        char buffer[50];
        array<double, 3> params = campaignData.getParameters();
        int charsWritten = sprintf(buffer, "%s#%g#%g#%g@", campaignId.c_str(), params[0], params[1], params[2]);
        if(charsWritten < 0)
            throw "Data stringify operation not succeeded!";
        std::string bufferString = buffer;
        return bufferString;
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////   Members of struct CampaignScore   /////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////

    CampaignScore::CampaignScore(string campaignParamString, const char delim) {
        vector<string> tokens = tokenizeString(campaignParamString, delim);
        assert(tokens.size() == 4);
        campaignId = tokens[0];
        double mu_0 = std::stod(tokens[1]);
        double alpha = std::stod(tokens[2]); 
        double beta = std::stod(tokens[3]);
        score = calculateCampaignScore(mu_0, alpha, beta);
    }

    double CampaignScore::calculateCampaignScore(double mu_0, double alpha, double beta) {
        auto timeCount = std::chrono::steady_clock::now().time_since_epoch().count();
        static default_random_engine generator(timeCount);
        gamma_distribution<double> gammaDistribution(alpha, 1.0 / beta);
        double precision = gammaDistribution(generator);
        if(precision == 0) 
            precision = 0.001;
        double estimatedVariance = 1.0 / precision;
        normal_distribution<double> normalDistribution(mu_0, sqrt(estimatedVariance));
        double gaussSample = normalDistribution(generator);
        return gaussSample;
    }

    ///////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////   Members of struct CampaignParams   ////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////

    CampaignParams::CampaignParams(std::string campaignDataString, const char delimCampaignData) {
        vector<string> campaignData = tokenizeString(campaignDataString, delimCampaignData);
        campaignId = campaignData[0];
        mu_0 = std::stod(campaignData[1]);
        alpha = std::stod(campaignData[2]); 
        beta = std::stod(campaignData[3]); 
    }

    ///////////////////////////////////////////////////////////////////////////////////////
    //////////////////////   Members of struct UserCampaignsMeans   ///////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////

    UserCampaignsMeans::UserCampaignsMeans(string _id, string userCampaignsString, const char delimCampaigns, const char delimCampaignData) {
        userId = _id;
        zeroMean = nonZeroMean = 0;
        meanOfAlphas = 0.0;
        vector<string> campaigns = tokenizeString(userCampaignsString, delimCampaigns);
        for(string &campaignString: campaigns) {
            vector<string> campaignData = tokenizeString(campaignString, delimCampaignData);
            double mu_0 = std::stod(campaignData[1]);
            double alpha = std::stod(campaignData[2]); 
            double beta = std::stod(campaignData[3]); 
            if(mu_0 > 0.0)
                nonZeroMean++;
            else
                zeroMean++;
            meanOfAlphas += alpha;
            meanOfBetas += beta;
        }
        meanOfAlphas /= nonZeroMean;
        meanOfBetas /= nonZeroMean;
    }

    ///////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////  Other methods   ///////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////

    vector<string> tokenizeString(string const str, const char delim) {
        vector<string> tokens;
        size_t start, end = 0;
        while ((start = str.find_first_not_of(delim, end)) != string::npos) {
            end = str.find(delim, start);
            tokens.push_back(str.substr(start, end - start));
        }
        return tokens;
    }

    double correctionFactor() {
        std::tm ti = std::tm();
        ti.tm_year = 122;
        ti.tm_mon = 8;
        ti.tm_mday = 1;
        std::time_t tt = std::mktime(&ti);

        system_clock::time_point tp = system_clock::from_time_t(tt);
        system_clock::duration d = system_clock::now() - tp;

        typedef duration<int, std::ratio<60*60*24>> daysType;
        daysType ndays = duration_cast<daysType>(d);
        double corr = (ndays.count() > 0) ? ndays.count() : 0;
        return corr;
    }
}