#include <unordered_map>

using namespace std;
using json = nlohmann::json;

namespace coinis {

    struct ThomsonSamplingParams {
        const double alphaInit;         // initial value of alpha parameter of gamma distribution
        const double betaInit;          // initial value of beta parameter of gamma distribution
        const double alphaScale;        // scaling factor in calculating alpha parameter (0.5 originally)
    };

    class CampaignData {
        private:
            int timeSteps;      // number of time steps (n in algorithm)
            vector<unsigned long> impressions;
            vector<double> income;
            double mu_0;        // estimated mean mu_0 of reward parameter (eCPM = income/impressions*1000)
            double alpha;       // alpha parameter of gamma distribution
            double beta;        // beta parameter of gamma distribution

        public:
            static int timeStepsMax;    // max number of time steps
            CampaignData();
            CampaignData(int);
            array<double, 3> getParameters();
            void insertData(unsigned long, double);
            string getCampaignDataString();
            void calculateCampaignParameters(const ThomsonSamplingParams);
            inline double calculateReward(double, double);
            void printCampaignParameters();
    };

    class CampaignUsers {
        private:
            unordered_map<string, unordered_map<string, CampaignData>> users;

        public:
            CampaignUsers();
            CampaignUsers(string);
            unordered_map<string, unordered_map<string, CampaignData>> &getMap();
            void fillFromDataset(string);
            void printUsers();
            void printAllUserCampaigns();
            void printCampaignsOfUser(string);
            void printCampaignDataOfAllUsers(string);
            void addNewUser(string, string);
            void addNewCampaign(string, string);
            void updateCampagnData(string, string, unsigned long, double);
            void calculateParametersOfAllCampaigns(const ThomsonSamplingParams);
            void printParametersOfAllCampaigns();
            void exportCampaigns(string);
            json createJsonData();
            json createJsonDataStringified();
            void printJsonToFile(json &, string);
            string stringifyCampaignData(string, CampaignData);
    };
    
    struct CampaignScore {
        public:
            string campaignId;
            double score;
        
            CampaignScore(string, const char);
            double calculateCampaignScore(double, double, double);
    };

    struct CampaignParams {
        public:
            string campaignId;
            double mu_0;
            double alpha;
            double beta;

        CampaignParams(string, const char delimCampaignData);
    };

    struct UserCampaignsMeans {
        public:
            string userId;
            double meanOfAlphas;
            double meanOfBetas;
            int zeroMean;
            int nonZeroMean;

        UserCampaignsMeans(string, string, const char, const char);
    };

    vector<string> tokenizeString(string const str, const char delim);
    double correctionFactor();
}