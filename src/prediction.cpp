#include <iostream>
#include <fstream>
#include <unordered_map>
#include <vector>
#include <chrono>
#include <math.h>
#include "include/json.hpp"
#include "include/campaigns.h"

using namespace std;
using namespace std::chrono;
using json = nlohmann::json;
using namespace coinis;


int main(int argc, char *argv[]) {
    
    auto startTime = steady_clock::now();

    //////////////////////////////////////////////////////////////////
    //////////////       Processing user's input        //////////////
    //////////////////////////////////////////////////////////////////
    if(argc != 6) {
        cerr << "Unexpected number/format of command line arguments!" << endl
             << "Run the application by typing its name and five parameters, as follows:" << endl
             << "prediction zone:1009631 browser:12 country:VN os:12 device:0" << endl << endl
             << "IMPORTANT: 1. Order of parameters can be arbitrary" << endl
             << "           2. Do not put space after :" << endl << endl;
        exit(100);
    }
    string zoneId, browserId, countryCode, osId, deviceId;
    for(short i = 1; i < 6; i++) {
        string temp(argv[i]);
        short separatorPos = temp.find(":");
        string parameterName = temp.substr(0, separatorPos);
        string parameterValue = temp.substr(separatorPos + 1);
        std::transform(parameterName.begin(), parameterName.end(), parameterName.begin(), 
                      [](unsigned char c){ return std::tolower(c); });     // Convert parameter name to lowercase
        if(parameterName == "zone")
            zoneId = parameterValue;
        else if(parameterName == "browser")
            browserId = parameterValue;
        else if(parameterName == "country")
            countryCode = parameterValue;
        else if(parameterName == "os")
            osId = parameterValue;
        else if(parameterName == "device")
            deviceId = parameterValue;
        else {
            cerr << "Undefined parameter name!" << endl
                 << "Allowed parameter names are \'zone\', \'browser\', \'country\', \'os\' and \'device\'." << endl << endl;
            exit(100);
        }
    }
    string userId = zoneId + "_" + browserId + "_" + countryCode + "_" + osId + "_" + deviceId;

    //////////////////////////////////////////////////////////////////
    ///////////////////         Prediction         ///////////////////
    //////////////////////////////////////////////////////////////////
    string inputFile = "results/userCampaigns.json";

    ifstream inputFileStream(inputFile);
    try {
        if(inputFileStream.fail()) throw inputFile;
    }
    catch(string exceptionMessage) {
        cerr << "\n" << exceptionMessage << " was not successfully opened!\nPlease check that the file exists." << endl << endl;
        exit(200);
    }

    json jsonUsers = json::parse(inputFileStream);
    string userCampaignsString = jsonUsers[userId];
    vector<string> tokens = tokenizeString(userCampaignsString, '@');
    vector<CampaignScore> campaignScores;
    for(string &campaignString: tokens)
        campaignScores.push_back(CampaignScore(campaignString, '#'));

    sort(campaignScores.begin(), campaignScores.end(), [](const CampaignScore& lhs, const CampaignScore& rhs) {
        return lhs.score > rhs.score;
    });

    //////////////////////////////////////////////////////////////////
    ///////////////////  Output optimal campaigns  ///////////////////
    //////////////////////////////////////////////////////////////////

    short selectedCampaignsNo = (campaignScores.size() < 8) ? campaignScores.size() : 8;
    cout << "\nOptimal campaigns: " << endl;
    for(short i = 0; i < selectedCampaignsNo; i++)
        cout << campaignScores[i].campaignId << endl;

    auto endTime = steady_clock::now();
    uint32_t elapsedMSecs = duration_cast<chrono::milliseconds>(endTime - startTime).count();
    cout << string(36, '-') << endl;
    cout << "  Total elapsed time: " << elapsedMSecs << " millisecs" << endl;
    cout << string(36, '-') << endl << endl;
    return 0;
}