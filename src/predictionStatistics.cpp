#include <iostream>
#include <fstream>
#include <filesystem>
#include "include/json.hpp"
#include "include/campaigns.h"

using namespace std;
using json = nlohmann::json;
using namespace coinis;
namespace fileSys = std::filesystem;


int main(int argc, char *argv[]) {
    //////////////////////   Processing user's command-line input   //////////////////////
    if(argc != 2) {
        cerr << "Unexpected number of command line arguments!" << endl;
        exit(100);
    }
    string userId = argv[1];
    string inputFile = "datasets/campaigns_" + userId + ".csv";
    if(fileSys::exists(inputFile) == 0) {
        cerr << "Dataset file not found. The program will terminate.\nPlease provide correct filename." << endl;
        exit(200);
    }
    /////////////////////////////////////////////////////////////////////////////////////
    ThomsonSamplingParams tsParams{1.0, 0.06, 0.5};
    
    CampaignUsers campaignUsers(inputFile);
    campaignUsers.calculateParametersOfAllCampaigns(tsParams);
    string jsonFilename = "results/userCampaigns" + userId + ".json";
    campaignUsers.exportCampaigns(jsonFilename);
    campaignUsers.printCampaignDataOfAllUsers("userCampaignsData.txt");

    ifstream inputFileStream(jsonFilename);
    json jsonUsers = json::parse(inputFileStream);
    json::iterator it = jsonUsers.begin();
    string userCampaignsString = jsonUsers[it.key()];

    auto comparisonScores = [](const CampaignScore& lhs, const CampaignScore& rhs) {
        return lhs.score > rhs.score;
    };
    auto comparisonSamples = [](const pair<string, unsigned long>& lhs, const pair<string, unsigned long>& rhs) {
        return lhs.second > rhs.second;
    };
    
    map<string, unsigned long> campaignSamplesMap;
    vector<string> campaignTokens = tokenizeString(userCampaignsString, '@');
    for(string &campaignString: campaignTokens) {
        vector<string> tokensTemp = tokenizeString(campaignString, '#');
        if(tokensTemp.size() != 4)
            throw "Not enough campaign parameters!";
        string campaignId = tokensTemp[0];
        campaignSamplesMap[campaignId] = 0;
    }
    // for (std::pair<std::string, unsigned long> element : campaignSamplesMap)
    //     cout << element.first << " " << element.second << endl;

    unsigned long sampNo = 100000;
    campaignTokens.clear();
    for(unsigned long i = 0; i < sampNo; i++) {
        campaignTokens = tokenizeString(userCampaignsString, '@');
        vector<CampaignScore> campaignScores;
        for(string &campaignString: campaignTokens)
            campaignScores.push_back(CampaignScore(campaignString, '#'));
        sort(campaignScores.begin(), campaignScores.end(), comparisonScores);
        campaignSamplesMap[campaignScores[0].campaignId]++;
    }

    vector<pair<string, unsigned long>> sortedVector;
    for (const auto &item: campaignSamplesMap)
        sortedVector.emplace_back(item);
    std::sort(sortedVector.begin(), sortedVector.end(), comparisonSamples);
    for (const auto &keyValue: sortedVector)
        cout << keyValue.first << " : " << (double) keyValue.second / sampNo * 100 << "%" << endl;
}