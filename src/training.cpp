#include <iostream>
#include <chrono>
#include <filesystem>
#include "include/csv.h"
#include "include/json.hpp"
#include "include/campaigns.h"

using namespace std;
using namespace std::chrono;
using namespace coinis;
namespace fileSys = std::filesystem;


int main(int argc, char *argv[]) {
    //////////////////////   Processing user's command-line input   //////////////////////
    if(argc != 2 && argc != 3) {
        cerr << "\nUnexpected number of command line arguments!" << endl << endl
             << "Run the application by typing its name and campaign dataset filename" << endl
             << "(including relative or apsolute path, without apostrophes or quotes)." << endl
             << "EXAMPLE:  training datasets/campaigns_all.csv" << endl << endl
             << "Optionally, you can define the third parameter - number of time steps (e.g. # of hours in the campaign dataset)." << endl
             << "EXAMPLE:  training datasets/campaigns_all.csv 48" << endl << endl;
        exit(100);
    }
    string inputFile = argv[1];
    if(fileSys::exists(inputFile) == 0) {
        cerr << "Dataset file not found. The program will terminate.\nPlease provide correct filename." << endl;
        exit(200);
    }
    if(argc == 3) {
        int tsMax;
        try {
            tsMax = std::stoi(argv[2]);
            if(tsMax < 1 || tsMax > 100)
                throw std::invalid_argument("");
            CampaignData::timeStepsMax = tsMax;
        }
        catch (std::invalid_argument& ia) {
	        std::cerr << "\nInvalid third parametar, which should be an integer within [1,100]." << endl
	                  << "Execution will proceed with default number of " << CampaignData::timeStepsMax << " time steps." << endl;
        }
    }
    //////////////////////////////////////////////////////////////////////////////////////

    if(fileSys::exists("results") == 0)
        fileSys::create_directory("results");
    
    auto startTime = steady_clock::now();
    
    ThomsonSamplingParams tsParams{1.0, 0.06, 0.5};

    cout << "\nDataset loading...   ";
    CampaignUsers campaignUsers(inputFile);
    cout << "Dataset loaded!" << endl;
    cout << "Model training...   ";
    campaignUsers.calculateParametersOfAllCampaigns(tsParams);
    cout << "Model trained!" << endl;
    string outputFile = "results/userCampaigns.json";
    campaignUsers.exportCampaigns(outputFile);
    cout << "File userCampaigns.json created in folder results/" << endl;
   
    auto endTime = steady_clock::now();
    uint32_t elapsedMSecs = duration_cast<chrono::milliseconds>(endTime - startTime).count();
    cout << string(50, '-') << endl;
    cout << "Total elapsed time: " << elapsedMSecs << " millisecs" << endl;
    cout << string(50, '-') << endl << endl;
    return 0;
}